
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'movie_bean.dart';
import '../toast_util.dart';
import 'movie_details.dart';
/**
 *
 * 创建人：xuqing
 * 创建时间：2020年8月2日16:25:52
 * 类说明：电影列表
 */

class Movielist extends StatefulWidget {
  final String mt;
  Movielist({Key key,this.mt}) : super(key: key);

  @override
  _MovielistState createState() {
    return _MovielistState();
  }

}

class _MovielistState extends State<Movielist> {
  int  page=1;
  int pagesize=5;
  var  mlist=[];
  var total=0;
  int  datasize=0;

  MovieBean movieBean;
  Subject _subject;
  List<Subject>data=new List();
  String loadMoreText;

  ScrollController _controller = new ScrollController();
  _MovielistState() {
    //固定写法，初始化滚动监听器，加载更多使用
    _controller.addListener(() {
      var maxScroll = _controller.position.maxScrollExtent;
      var pixel = _controller.position.pixels;
      if (maxScroll == pixel&& data.length < total&&datasize<=5) {
          loadMoreText = "正在加载中...";
          ToastUtil.showInfo(context, loadMoreText);
          getMovielist();
      } else {
          loadMoreText = "没有更多数据";
          ToastUtil.showInfo(context, loadMoreText);
      }
    });
  }


  @override
  void initState() {
    super.initState();
    getMovielist();
  }

  @override
  void dispose() {
    super.dispose();
  }
  getMovielist()async {
    this.page++;
    int  offset=(page-1)*pagesize;
    Dio dio=new Dio();
    Response response=await dio.get("http://www.liulongbin.top:3005/api/v2/movie/${widget.mt}?start=$offset&count=$pagesize");
     setState(() {
       var result=response.data.toString();
       print("result    ----   首次请求数据   ---   >  "+result);
       movieBean =MovieBean.fromJson(response.data);
       data.addAll(movieBean.subjects);
       total=movieBean.total;
       datasize=movieBean.subjects.length;
       print("datasize   ---  >  "+datasize.toString());


     });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return data.length==0?
        new Center(child: new CircularProgressIndicator())
        :Container(
        child:RefreshIndicator(
          onRefresh:_refresh ,
          child: ListView.builder(
            controller: _controller, //指明控制器加载更多使用
            itemCount: data==null?0:data.length,
            itemBuilder: (BuildContext context, int position){
                return itemWidget(position);
            },
          ),
        )
    );
  }

  Future  _refresh()async  {
    print("上拉加载");
      page=0;
      data.clear();
      getMovielist();
   return null;
  }

  /**
   * item布局
   *
   */
  Widget  itemWidget(int index){
    _subject=data[index];
    Rating rating=_subject.rating;
    if(_subject==null){
      return Container(
        child: Center(
          child: Text("暂时没有数据",style: TextStyle(
              color: Colors.red,fontSize: 30
          ),),
        ),
      );
    }else{
      return GestureDetector(
        onTap: (){
          setState(() {
            Navigator.push(context, MaterialPageRoute(
              builder: (BuildContext context){
                return MovieDetails(id: _subject.id,detail: _subject.title,);

              }
            ));

          });
        },
        child:Container(
          height: 200,
          decoration: BoxDecoration(color: Colors.white,border: Border(
            top: BorderSide(
              color: Colors.black
            )
          )),
          child:Row(
        children: <Widget>[
        Image.network(_subject.images.small,
          width: 130,
          height: 180,
          fit: BoxFit.fill,),
        Container(
          height: 200,
          child:Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("电影名称:"+_subject.title),
              Text("上映年份"+_subject.year),
              Text("电影类型:"+_subject.genres.join(",")),
              Text("豆瓣评分:"+_subject.rating.average+"分"),
              Row(
                children: <Widget>[

                  Text("主要演员"),
                  Container(
                    width: 30,
                    height: 30,
                    child: Image.network(_subject.images.small),
                  ),
                  Container(
                    width: 30,
                    height: 30,
                    child: Image.network(_subject.images.large),
                  ),
                  Container(
                    width: 30,
                    height: 30,
                    child:  Image.network(_subject.images.medium)
                  )

                ],
              )
            ],
          ),
        )
        ],
        ) ,
     )
    );
    }
  }

}