 





class MovieBean {
    int count;
    int start;
    List<Subject> subjects;
    String title;
    int total;

    MovieBean({this.count, this.start, this.subjects, this.title, this.total});
    factory MovieBean.fromJson(Map<String, dynamic> json) {
        return MovieBean(
            count: json['count'],
            start: json['start'],
            subjects: json['subjects'] != null ? (json['subjects'] as List).map((i) => Subject.fromJson(i)).toList() : null,
            title: json['title'],
            total: json['total'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['start'] = this.start;
        data['title'] = this.title;
        data['total'] = this.total;
        if (this.subjects != null) {
            data['subjects'] = this.subjects.map((v) => v.toJson()).toList();
        }
        return data;
    }
}

class Subject {
    String alt;
    List<Cast> casts;
    int collect_count;
    List<Director> directors;
    List<String> genres;
    String id;
    Images images;
    String original_title;
    Rating rating;
    String subtype;
    String title;
    String year;

    Subject({this.alt, this.casts, this.collect_count, this.directors, this.genres, this.id, this.images, this.original_title, this.rating, this.subtype, this.title, this.year});

    factory Subject.fromJson(Map<String, dynamic> json) {
        return Subject(
            alt: json['alt'],
            casts: json['casts'] != null ? (json['casts'] as List).map((i) => Cast.fromJson(i)).toList() : null,
            collect_count: json['collect_count'],
            directors: json['directors'] != null ? (json['directors'] as List).map((i) => Director.fromJson(i)).toList() : null,
            genres: json['genres'] != null ? new List<String>.from(json['genres']) : null,
            id: json['id'],
            images: json['images'] != null ? Images.fromJson(json['images']) : null,
            original_title: json['original_title'],
            rating: json['rating'] != null ? Rating.fromJson(json['rating']) : null,
            subtype: json['subtype'],
            title: json['title'],
            year: json['year'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['alt'] = this.alt;
        data['collect_count'] = this.collect_count;
        data['id'] = this.id;
        data['original_title'] = this.original_title;
        data['subtype'] = this.subtype;
        data['title'] = this.title;
        data['year'] = this.year;
        if (this.casts != null) {
            data['casts'] = this.casts.map((v) => v.toJson()).toList();
        }
        if (this.directors != null) {
            data['directors'] = this.directors.map((v) => v.toJson()).toList();
        }
        if (this.genres != null) {
            data['genres'] = this.genres;
        }
        if (this.images != null) {
            data['images'] = this.images.toJson();
        }
        if (this.rating != null) {
            data['rating'] = this.rating.toJson();
        }
        return data;
    }
}

class Cast {
    String alt;
    Avatars avatars;
    String id;
    String name;

    Cast({this.alt, this.avatars, this.id, this.name});

    factory Cast.fromJson(Map<String, dynamic> json) {
        return Cast(
            alt: json['alt'],
            avatars: json['avatars'] != null ? Avatars.fromJson(json['avatars']) : null,
            id: json['id'],
            name: json['name'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['alt'] = this.alt;
        data['id'] = this.id;
        data['name'] = this.name;
        if (this.avatars != null) {
            data['avatars'] = this.avatars.toJson();
        }
        return data;
    }
}

class Avatars {
    String large;
    String medium;
    String small;

    Avatars({this.large, this.medium, this.small});

    factory Avatars.fromJson(Map<String, dynamic> json) {
        return Avatars(
            large: json['large'],
            medium: json['medium'],
            small: json['small'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['large'] = this.large;
        data['medium'] = this.medium;
        data['small'] = this.small;
        return data;
    }
}

class Images {
    String large;
    String medium;
    String small;

    Images({this.large, this.medium, this.small});

    factory Images.fromJson(Map<String, dynamic> json) {
        return Images(
            large: json['large'],
            medium: json['medium'],
            small: json['small'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['large'] = this.large;
        data['medium'] = this.medium;
        data['small'] = this.small;
        return data;
    }
}

class Director {
    String alt;
    AvatarsX avatars;
    String id;
    String name;
    Director({this.alt, this.avatars, this.id, this.name});

    factory Director.fromJson(Map<String, dynamic> json) {
        return Director(
            alt: json['alt'],
            avatars: json['avatars'] != null ? AvatarsX.fromJson(json['avatars']) : null,
            id: json['id'],
            name: json['name'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['alt'] = this.alt;
        data['id'] = this.id;
        data['name'] = this.name;
        if (this.avatars != null) {
            data['avatars'] = this.avatars.toJson();
        }
        return data;
    }
}

class AvatarsX {
    String large;
    String medium;
    String small;

    AvatarsX({this.large, this.medium, this.small});

    factory AvatarsX.fromJson(Map<String, dynamic> json) {
        return AvatarsX(
            large: json['large'],
            medium: json['medium'],
            small: json['small'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['large'] = this.large;
        data['medium'] = this.medium;
        data['small'] = this.small;
        return data;
    }
}

class Rating {
    String  average;
    int max;
    int min;
    String stars;

    Rating({ this.max, this.min, this.stars,this.average});

    factory Rating.fromJson(Map<String, dynamic> json) {
        return Rating(
            average: json['average'].toString(),
            max: json['max'],
            min: json['min'],
            stars: json['stars'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['average']= this.average;
        data['max'] = this.max;
        data['min'] = this.min;
        data['stars'] = this.stars;
        return data;
    }
}